Require Import bedrock.bytestring.

(** To perform efficient pretty printing, one needs to use a tree structure 
  to avoid quadratic overhead of appending strings. *)
Module Tree.
  Local Open Scope bs_scope.
  Inductive t := 
  | string : Bytestring.t -> t
  | append : t -> t -> t.

  Coercion string : Bytestring.t >-> t.

  Infix "++" := append.

  Fixpoint to_rev_list_aux t acc :=
    match t with
    | string s => cons s acc
    | append s s' => to_rev_list_aux s' (to_rev_list_aux s acc)
    end.
  
  Fixpoint to_string_acc acc l := 
    match l with
    | nil => acc
    | cons s xs => to_string_acc (Bytestring.append s acc) xs
    end.

  Definition to_string t := 
    let l := to_rev_list_aux t nil in
    to_string_acc "" l.
  
  Definition string_of_list_aux {A} (f : A -> t) (sep : t) (l : list A) :=
    let fix aux l :=
        match l return t with
        | nil => ""
        | cons a nil => f a
        | cons a l => f a ++ sep ++ aux l
      end
    in aux l.

  Definition string_of_list {A} (f : A -> t) l :=
    "[" ++ string_of_list_aux f "," l ++ "]".

  Definition print_list {A} (f : A -> t) (sep : t) (l : list A) : t :=
    string_of_list_aux f sep l.
    
  Fixpoint concat (sep : t) (s : list t) : t :=
    match s with
    | nil => Bytestring.EmptyString
    | cons s nil => s
    | cons s xs => s ++ sep ++ concat sep xs
    end.

  Definition parens (top : bool) (s : t) :=
    if top then s else "(" ++ s ++ ")".
    
End Tree.